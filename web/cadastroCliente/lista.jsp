<%@page import="br.com.senac.modelo.Cliente"%>
<%@page import="java.util.List"%>
<%@page import="br.com.senac.banco.ClienteDAO"%>
<jsp:include page="../header.jsp" />

<%
    ClienteDAO dao = new ClienteDAO();
    List<Cliente> lista = dao.listarTodos();

%>


<div class="container">
    <fieldset>
        <legend>Lista de Clientes</legend> 

        <table class="table table-hover">
            <thead>
                <tr>
                    <td>C�digo</td><td>Primeiro Nome</td><td>�ltimo Nome</td><td>Endere�o</td><td>Telefone</td>
                </tr>
            </thead>
            <tbody>
                <% for (Cliente cli : lista) {%>
                <tr>
                    <td><%= cli.getCodigo() %></td>
                    <td><%= cli.getPrimeiroNome()%></td>
                    <td><%= cli.getUltimoNome()%> </td>
                    <td><%= cli.getEndereco() %> </td>
                    <td><%= cli.getEndereco().getTelefone() %> </td>
                </tr>
                <%}%>
            </tbody>
        </table>
    </fieldset>
</div>


<jsp:include page="../footer.jsp" />
