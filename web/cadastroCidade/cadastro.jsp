<%-- 
    Document   : cadastro
    Created on : 24/10/2017, 16:06:14
    Author     : jordy.allan
--%>

<jsp:include page="../header.jsp" />


<div class="container">

    <fieldset>
        <legend>Cadastro de Cidade</legend>

        <form class="form-horizontal" action="./cidade.do" method="post">

            <input type="hidden" name="codigo" value="0" />

            <div class="form-group">
                <label class="control-label col-sm-2" for="codigo">C�digo:</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" id="codigo"  readonly="true"  value="${cidade.codigo}" />
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2" for="cidade">Cidade:</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="cidade" name="cidade" required="true" value="${cidade.nome}" />
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2" for="pais">Pa�s:</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="pais" name="pais" required="true" ${cidade.pais.nome} />
                </div>
            </div>

            <div class="form-group">

                <div class="col-sm-offset-2 col-sm-10">
                    <input type="submit" value="Salvar" class="btn btn-primary" />
                    <input type="reset" value="Cancelar" class="btn btn-danger" />

                </div>
            </div>
        </form>
    </fieldset>
</div>

<jsp:include page="../footer.jsp" /> 
