<%@page import="br.com.senac.modelo.Cidade"%>
<%@page import="java.util.List"%>
<%@page import="br.com.senac.banco.CidadeDAO"%>
<jsp:include page="../header.jsp" />

<%
    CidadeDAO dao = new CidadeDAO();
    List<Cidade> lista = dao.listarTodos();

%>


<div class="container">
    <fieldset>
        <legend>Lista de Cidades</legend> 

        <table class="table table-hover">
            <thead>
                <tr>
                    <td>C�digo</td><td>Nome</td><td>Pais</td>
                </tr>
            </thead>
            <tbody>
                <% for (Cidade ci : lista) {%>
                <tr>
                    <td><%= ci.getCodigo() %></td>
                    <td><%= ci.getNome()%></td>
                    <td><%= ci.getPais().getNome() %> </td>
                </tr>
                <%}%>
            </tbody>
        </table>
    </fieldset>
</div>


<jsp:include page="../footer.jsp" />

