
<jsp:include page="../header.jsp" />



<div class="container">
    <fieldset>
        <legend>Cadastro de Atores</legend>

        <form class="form-horizontal" >
            <div class="form-group">
                <label class="control-label col-sm-2" for="codigo">C�digo:</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" id="codigo" name="codigo">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="primeiroNome">Primeiro nome:</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="primeiroNome" placeholder="Entre com o primeiro nome" name="primeiroNome">
                </div>
                <label class="control-label col-sm-2" for="ultimoNome">Ultimo nome:</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="ultimoNome" placeholder="Entre com o Ultimo nome" name="ultimoNome">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-12">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12"> 
                            <input type="submit"   class="btn btn-primary col-" value="Salvar" />
                            <input type="reset"    class="btn btn-danger" value="Cancelar" />
                        </div>
                    </div>

                </div>
            </div>
        </form> 

    </fieldset>


</div>



<%
    if (request.getAttribute("mensagem") != null) {
        out.print(request.getAttribute("mensagem"));
    } else {
        out.print("");
    }
%>


<jsp:include page="../footer.jsp" />