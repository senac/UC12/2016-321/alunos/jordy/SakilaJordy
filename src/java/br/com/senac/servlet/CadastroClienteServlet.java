/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.servlet;

import br.com.senac.banco.CidadeDAO;
import br.com.senac.banco.ClienteDAO;
import br.com.senac.modelo.Cidade;
import br.com.senac.modelo.Cliente;
import br.com.senac.modelo.Endereco;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jordy.allan
 */

@WebServlet(name = "CadastroClienteServlet", urlPatterns = {"/cadastroCliente/cliente.do"})
public class CadastroClienteServlet extends HttpServlet {

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest (request, response);
        
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int codigo = Integer.parseInt(request.getParameter("codigo"));
        String primeiroNome = request.getParameter("primeiroNome");
        String ultimoNome = request.getParameter("ultimoNome");
        String email = request.getParameter("email");
        String telefone = request.getParameter("telefone");
        String cep = request.getParameter("cep");
        String logradouro = request.getParameter("endereco");
        String complemento = request.getParameter("complemento");
        String distrito = request.getParameter("distrito");
        int codigoCidade = Integer.parseInt(request.getParameter("cidade"));

        Cliente cliente = new Cliente();
        cliente.setCodigo(codigo);
        cliente.setPrimeiroNome(primeiroNome);
        cliente.setUltimoNome(ultimoNome);
        cliente.setEmail(email);

        Endereco endereco = new Endereco();
        endereco.setLogradouro(logradouro);
        endereco.setComplemento(complemento);
        endereco.setDistrito(distrito);
        endereco.setTelefone(telefone);
        endereco.setCep(cep);

        CidadeDAO cidadeDao = new CidadeDAO();

        Cidade c = cidadeDao.buscarPorId(codigoCidade);

        endereco.setCidade(c);

        cliente.setEndereco(endereco);

        ClienteDAO dao = new ClienteDAO();

        dao.salvar(cliente);

        RequestDispatcher dispatcher = request.getRequestDispatcher("./cadastro.jsp");
        
        request.setAttribute("cliente", cliente);

        dispatcher.forward(request, response);
       
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

