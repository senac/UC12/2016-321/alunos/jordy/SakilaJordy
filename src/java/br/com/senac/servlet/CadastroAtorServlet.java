/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.servlet;

import br.com.senac.banco.AtorDAO;
import br.com.senac.modelo.Ator;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jordy.allan
 */
public class CadastroAtorServlet extends HttpServlet {

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String mensagem = null;
        try {

            int codigo = Integer.parseInt(request.getParameter("codigo"));
            String primeiroNome = request.getParameter("primeiroNome");
            String ultimoNome = request.getParameter("ultimoNome");

           validarParametros(primeiroNome , ultimoNome);
           
            Ator ator = new Ator(codigo, primeiroNome, ultimoNome);
            
            salvar(ator);
            
            mensagem = "Salvo com sucesso!!";
            
            
        } catch (NumberFormatException ex) {
            mensagem = "Somente é premitido números no campo código";
        } catch (Exception ex) {
            mensagem = ex.getMessage();
        } finally {
            request.setAttribute("mensagem", mensagem);
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("./cadastro.jsp");
        
        dispatcher.forward(request, response);

    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }

    private void validarParametros(String primeiroNome, String ultimoNome) throws Exception {
        
        StringBuilder sb = new StringBuilder(); 
        
        if(primeiroNome.trim().equals("") ){
            sb.append("Primeiro Nome.<br />"); 
        }
        
        if(ultimoNome.trim().equals("") ){
            sb.append("Ultimo Nome.<br />"); 
        }
        
        if(sb.toString().length() > 0 ){
            sb.insert(0, "Favor preencher os campos abaixo:<br />"); 
            
            throw new Exception(sb.toString());
        }
        
        
    }

    private void salvar(Ator ator) {
        AtorDAO dao = new AtorDAO() ; 
        
        if(ator.getCodigo() == 0 ){
            dao.salvar(ator);
        }else{
            dao.atualizar(ator);
        }
    }

}
