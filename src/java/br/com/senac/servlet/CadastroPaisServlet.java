/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.servlet;

import br.com.senac.banco.Conexao;
import br.com.senac.banco.PaisDAO;
import br.com.senac.modelo.Pais;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jordy.allan
 */
public class CadastroPaisServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        
        int codigo = Integer.parseInt(request.getParameter("codigo"));
        String nome = request.getParameter("nome");

        
        HttpSession session = request.getSession();
        
        Pais p = (Pais) session.getAttribute("pais");      
        
 
        Pais pais = new Pais();
        pais.setCodigo(codigo);
        pais.setNome(nome);

        
        PaisDAO dao = new PaisDAO();

        if (pais.getCodigo() == 0) {
            dao.salvar(pais);
        } else {
            dao.atualizar(pais);
        }
      
      
        RequestDispatcher dispatcher = request.getRequestDispatcher("./lista.jsp");
        
        dispatcher.forward(request, response);

    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
