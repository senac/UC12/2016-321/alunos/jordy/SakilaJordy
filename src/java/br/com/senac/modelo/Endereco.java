
package br.com.senac.modelo;


public class Endereco {
    
    private int codigo;
    private String logradouro;
    private String complemento;
    private String distrito;
    private String cep;
    private Cidade cidade;
    private String telefone;

    public Endereco() {
    }

    public Endereco(int codigo, String logradouro, String complemento, String distrito, String cep, Cidade cidade, String telefone) {
        this.codigo = codigo;
        this.logradouro = logradouro;
        this.complemento = complemento;
        this.distrito = distrito;
        this.cep = cep;
        this.cidade = cidade;
        this.telefone = telefone;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    
    @Override
    public String toString(){
        return  "Cep: " +  cep +  " - " + logradouro + " , " +  distrito + " "
                + complemento +  cidade.getNome() +  " - " + cidade.getPais().getNome()  ;
    }
    
 
}
