package br.com.senac.banco;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {

    private static final String url = "jdbc:mysql://localhost/sakila";
    private static final String user = "root";
    private static final String password = "123456";
    private static final String DRIVER = "com.mysql.jdbc.Driver";

    public static Connection getConnection() {

        Connection connection = null;

        try {
            
            Class.forName(DRIVER);

             
            connection = DriverManager.getConnection(url, user, password);

             
            System.out.println("Conectado com sucesso!!!");

        } catch (ClassNotFoundException ex) {
            System.out.println("Falha ao carregar o Driver do banco.");
        } catch (SQLException ex) {
            System.out.println("Falha ao conectar ao banco.");
        }
        return connection;
    }
    
    public static void main(String... a){
        Conexao.getConnection() ; 
    }

}
