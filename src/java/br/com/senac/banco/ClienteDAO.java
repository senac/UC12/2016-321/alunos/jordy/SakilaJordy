/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.banco;

import br.com.senac.modelo.Cidade;
import br.com.senac.modelo.Cliente;
import br.com.senac.modelo.Endereco;
import br.com.senac.modelo.Pais;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jordy.allan
 */
public class ClienteDAO implements DAO<Cliente>{

    @Override
    public void salvar(Cliente cliente) {
        
        Connection connection = null;
        
        try{
            
                connection = Conexao.getConnection();
                        
                connection.setAutoCommit(false);
                
                String query = "INSERT INTO address (address,address2  ,district  ,city_id  ,postal_code  ,phone )VALUES(?,?,?,?,?,?);";
                
                PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, cliente.getEndereco().getLogradouro());
            ps.setString(2, cliente.getEndereco().getComplemento());
            ps.setString(3, cliente.getEndereco().getDistrito());
            ps.setInt(4, cliente.getEndereco().getCidade().getCodigo());
            ps.setString(5, cliente.getEndereco().getCep());
            ps.setString(6, cliente.getEndereco().getTelefone());

            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();

            rs.first();

            int codigoEndereco = rs.getInt(1);
            cliente.getEndereco().setCodigo(codigoEndereco);

            query = "INSERT INTO  customer ( first_name ,last_name ,email ,address_id)VALUES(?,?,?,?)";

            ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

            ps.setString(1, cliente.getPrimeiroNome());
            ps.setString(2, cliente.getUltimoNome());
            ps.setString(3, cliente.getEmail());
            ps.setInt(4, cliente.getEndereco().getCodigo());

            ps.executeUpdate();

            rs = ps.getGeneratedKeys();

            rs.first();

            int codidoCliente = rs.getInt(1);

            cliente.setCodigo(codidoCliente);

            connection.commit();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());

            try {
                connection.rollback();
            } catch (SQLException ex1) {
                System.out.println(ex1.getMessage());
            }

        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

        }

    @Override
    public void atualizar(Cliente objeto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deletar(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Cliente> listarTodos() {
        Connection connection = null;
        List<Cliente> lista = new ArrayList<>();

        try {

            
            connection = Conexao.getConnection();
            
            
            
            String query = "Select  "
                    + "c.customer_id as codigo , "
                    + "c.first_name as primeiroNome ,  "
                    + "c.last_name as ultimoNome ,  "
                    + "c.email  ,  "
                    + "a.address_id as codigoEndereco ,  "
                    + "a.postal_code as cep ,  "
                    + "a.address as logradouro ,  "
                    + "a.district as distrito ,  "
                    + "a.address2 as complemento  ,  "
                    + "a.phone as telefone,  "
                    + "cc.city_id as codigoCidade, "
                    + "cc.city as nomeCidade  ,  "
                    + "p.country_id as codigoPais ,  "
                    + "p.country as pais "
                    + " "
                    + "from customer c "
                    + "inner join address a  "
                    + "on c.address_id = a.address_id "
                    + "inner join city cc  "
                    + "on a.city_id = cc.city_id "
                    + "inner join country p  "
                    + "on cc.country_id = p.country_id "
                    + " ;" ; 
            
            Statement statement = connection.createStatement() ; 
            
            ResultSet rs =  statement.executeQuery(query); 
            
            
            while(rs.next()){
                
                int codigo = rs.getInt("codigo"); 
                String primeiroNome = rs.getString("primeiroNome") ; 
                String ultimoNome = rs.getString("ultimoNome") ; 
                String email = rs.getString("email") ; 
                int codigoEndereco = rs.getInt("codigoEndereco") ; 
                String cep = rs.getString("cep") ; 
                String logradouro = rs.getString("logradouro") ; 
                String distrito = rs.getString("distrito") ; 
                String complemento = rs.getString("complemento") ; 
                String telefone = rs.getString("telefone") ; 
                int codigoCidade = rs.getInt("codigoCidade") ; 
                String nomeCidade = rs.getString("nomeCidade") ; 
                int codigoPais = rs.getInt("codigoPais") ; 
                String pais = rs.getString("pais") ; 
                
                Pais p = new Pais(codigoPais, pais) ; 
                Cidade c =  new Cidade(codigoCidade, nomeCidade, p);
                Endereco e = new Endereco(codigoEndereco, logradouro, complemento, distrito, cep, c, telefone) ; 
                Cliente cliente = new Cliente(codigo, primeiroNome, ultimoNome, email, true, e);
              
                lista.add(cliente);

            }

            
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

        return lista;
    }
    
    public Cliente buscarPorId(int id){
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
       
       
    }
