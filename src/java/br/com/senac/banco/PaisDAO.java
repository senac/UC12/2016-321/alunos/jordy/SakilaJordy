package br.com.senac.banco;

import br.com.senac.modelo.Pais;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PaisDAO implements DAO<Pais> {

    @Override
    public void salvar(Pais pais) {

        Connection connection = null;
        try {

           
            connection = Conexao.getConnection();

            String query = "INSERT INTO country(country) values('" + pais.getNome() + "') ; ";

            Statement statement = connection.createStatement();

            int resultado = statement.executeUpdate(query);

            if (resultado == 1) {
                System.out.println("Salvou com sucesso....");
            } else {
                System.out.println("Alguma coisa deu errado...");
            }

        } catch (SQLException ex) {
            System.out.println("Falha ao realizar operacao");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar o banco");
            }
        }

    }

    @Override
    public void atualizar(Pais pais) {

        Connection connection = null;
        try {
            connection = Conexao.getConnection();

            String query = "UPDATE country SET country = ? WHERE country_ID = ? ;";

            PreparedStatement ps = connection.prepareStatement(query);

            ps.setString(1, pais.getNome());
            ps.setInt(2, pais.getCodigo());

            ps.executeUpdate();

        } catch (SQLException ex) {
            System.err.println("Erro ao atualizar ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar o banco");
            }
        }

    }

    @Override
    public void deletar(int id) {

        Connection connection = null;
        try {
            connection = Conexao.getConnection();

            String query = "DELETE FROM country WHERE country_ID = ? ;";

            PreparedStatement ps = connection.prepareStatement(query);

            ps.setInt(1, id);

            ps.executeUpdate();

        } catch (SQLException ex) {
            System.err.println("Erro ao atualizar ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar o banco");
            }
        }

    }

    @Override
    public List<Pais> listarTodos() {

        List<Pais> lista = new ArrayList<>() ; 
        
        Connection connection = null;
        try {
            connection = Conexao.getConnection();

            String query = "SELECT * FROM country ; ";

            Statement st = connection.createStatement();

            ResultSet rs =  st.executeQuery(query) ; 
            
            while (rs.next()) {                
                Pais pais = new Pais() ; 
                
                int codigo = rs.getInt("country_ID");
                String nome = rs.getString("country") ; 
                
                pais.setCodigo(codigo);
                pais.setNome(nome);
                
                lista.add(pais) ; 
            }

        } catch (SQLException ex) {
            System.err.println("Erro ao atualizar ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar o banco");
            }
        }
        
        return lista ; 

    }

    @Override
    public Pais buscarPorId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
